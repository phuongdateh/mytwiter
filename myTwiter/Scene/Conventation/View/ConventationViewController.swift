//
//  ConventationViewController.swift
//  myTwiter
//
//  Created by DoanDuyPhuong on 4/20/20.
//  Copyright © 2020 DoanDuyPhuong. All rights reserved.
//

import Foundation
import UIKit

class ConventationViewController: UIViewController {
    
    // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    func setupUI() {
        setupNavigationBar()
    }
    
    func setupNavigationBar() {
        navigationItem.title = "Conventation"
    }
}

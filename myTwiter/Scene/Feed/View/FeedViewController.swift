//
//  FeedViewController.swift
//  myTwiter
//
//  Created by DoanDuyPhuong on 4/20/20.
//  Copyright © 2020 DoanDuyPhuong. All rights reserved.
//

import Foundation
import UIKit

class FeedViewController: UIViewController {
    
    private var createNewFeedButton: UIButton = UIButton()
    
    // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    func setupUI() {
        view.backgroundColor = .white
        setupNavigationBar()
        setupCreateNewFeedButton()
    }
    
    func setupNavigationBar() {
        let logoImageView = UIImageView.init(image: UIImage.init(named: Constant.logo))
        logoImageView.contentMode = .scaleAspectFit
        navigationItem.titleView = logoImageView
    }
    
    func setupCreateNewFeedButton() {
        createNewFeedButton.backgroundColor = .blue
        createNewFeedButton.tintColor = .white
        createNewFeedButton.setImage(UIImage.init(named: Constant.newFeed), for: .normal)
        view.addSubview(createNewFeedButton)
        
        createNewFeedButton.translatesAutoresizingMaskIntoConstraints = false
        createNewFeedButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -15).isActive = true
        createNewFeedButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -15).isActive = true
        createNewFeedButton.heightAnchor.constraint(equalToConstant: 56).isActive = true
        createNewFeedButton.widthAnchor.constraint(equalToConstant: 56).isActive = true
        
        createNewFeedButton.layer.masksToBounds = true
        createNewFeedButton.layer.cornerRadius = 56/2
    }
}

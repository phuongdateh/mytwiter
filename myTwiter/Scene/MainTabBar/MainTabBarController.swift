//
//  MainTabBarController.swift
//  myTwiter
//
//  Created by DoanDuyPhuong on 4/20/20.
//  Copyright © 2020 DoanDuyPhuong. All rights reserved.
//

import Foundation
import UIKit

class MainTabBarController: UITabBarController {
    
    // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setuptViewController()
    }
    
    // MARK: - Setup ViewController
    fileprivate func setuptViewController() {
        let feedNav = customUINavigationController(nameImage: Constant.homeTabBarIcon, rootViewController: FeedViewController())
        let exploreNav = customUINavigationController(nameImage: Constant.exploreTabBarIcon, rootViewController: ExploreViewController())
        let notificationNav = customUINavigationController(nameImage: Constant.notificationTabBarIcon, rootViewController: NotificationViewController())
        let conventationNav = customUINavigationController(nameImage: Constant.conventationTabBarIcon, rootViewController: ConventationViewController())
        
        viewControllers = [feedNav, exploreNav, notificationNav, conventationNav]
    }
    
    func customUINavigationController(nameImage: String, rootViewController: UIViewController) -> UINavigationController {
        let nav = UINavigationController.init(rootViewController: rootViewController)
        nav.tabBarItem.image = UIImage.init(named: nameImage)
        nav.navigationBar.tintColor = .white
        return nav
    }
}

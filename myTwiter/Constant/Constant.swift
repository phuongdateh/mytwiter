//
//  Constant.swift
//  myTwiter
//
//  Created by DoanDuyPhuong on 4/21/20.
//  Copyright © 2020 DoanDuyPhuong. All rights reserved.
//

import Foundation
    

public class Constant {
    
    // MARK: - Name TabBar Icon
    static let homeTabBarIcon: String = "home_unselected"
    static let exploreTabBarIcon: String = "search_unselected"
    static let notificationTabBarIcon: String = "like_unselected"
    static let conventationTabBarIcon: String = "ic_mail_outline_white_2x-1"
    static let logo: String = "twitter_logo_blue"
    static let newFeed: String = "new_tweet"
    
}
